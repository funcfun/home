#!/bin/sh
# Launcher for websites I commonly visit with a browser

browser=qutebrowser

sites=(
	"archlabs-forum      https://forum.archlabslinux.com/"
	"wayland-protocols   https://gitlab.freedesktop.org/wayland/wayland-protocols/"
	"river-irc-logs      https://libera.irclog.whitequark.org/river"
	"r/postmarketos      https://old.reddit.com/r/postmarketOS/"
	"r/zig               https://old.reddit.com/r/zig/new"
	"r/linux             https://old.reddit.com/r/linux/new"
	"r/unixporn          https://old.reddit.com/r/unixporn/new"
	"r/swaywm            https://old.reddit.com/r/swaywm/new"
	"r/wayland           https://old.reddit.com/r/wayland/new"
	"r/programming       https://old.reddit.com/r/programming"
	"github              https://github.com"
	"river               https://github.com/ifreund/river"
	"sourcehut           https://sr.ht"
	"deepl               https://deepl.com"
	"wikipedia           https://wikipedia.com"
	"drewdevault         https://drewdevault.com"
	"zig-docs            https://ziglang.org/documentation/0.8.0/"
	"zig-news            https://zig.news/"
	"linux-org-ru        https://linux.org.ru/"
	"glib/gtk-book       https://people.gnome.org/~swilmet/glib-gtk-book/"
)

# Let the last command of a pipeline run in the current environment. This allows
# read to set variables, so less subshells have to be used.
shopt -s lastpipe

# Name selector
for site in "${sites[@]}"
do
	echo "${site}" | cut -d' ' -f1
done | fzf -d' ' -e -i --color=bw --prompt='site: ' | read name
[ -z "${name}" ] && exit 1

# Get URL
for site in "${sites[@]}"
do
	_name="$(echo "${site}" | cut -d' ' -f1)"
	if [ "${_name}" == "${name}" ]
	then
		# Using awk here because cut is not smart enough for a string
		# with an arbitrary amount of spaces as delimiter. However to
		# get the first field cut is used because it is simply a lot
		# faster.
		url="$(echo "${site}" | awk '{print $2}')"
	fi
done
[ -z "${url}" ] && exit 1

command="${browser} \"${url}\""
setsid /bin/sh -c "${command}" >&/dev/null &
sleep 0.1

