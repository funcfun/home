set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/.bin $HOME/src/node_modules/.bin $fish_user_paths

set fish_greeting              # Supresses fish's intro message
set TERM "xterm-256color"      # Sets the terminal type
set EDITOR "lvim"              # $EDITOR use Emacs in terminal
set VISUAL "lvim"              # $VISUAL use Emacs in GUI mode

alias v='lvim'
alias vim='nvim'
alias g='git'

# git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias tag='git tag'
alias newtag='git tag -a'

# get error messages from journalctl
alias jctl="journalctl -p 3 -xb"
alias ytv-best="youtube-dl -f bestvideo+bestaudio "

### SET MANPAGER
### Uncomment only one of these!

### "bat" as manpager
# set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

### "nvim" as manpager
# set -x MANPAGER "vim -c 'set ft=man' -"

if status is-interactive
    # Commands to run in interactive sessions can go here
end


# TokyoNight Color Palette
set -l foreground 3760bf
set -l selection 99a7df
set -l comment 848cb5
set -l red f52a65
set -l orange b15c00
set -l yellow 8c6c3e
set -l green 587539
set -l purple 7847bd
set -l cyan 007197
set -l pink 9854f1

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment
