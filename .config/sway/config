# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term foot

set $uifont "pango:monospace 10"
set $highlight #6699CC
set $prompt #99c794
set $background #1B2B34
set $foreground #C0C5CE
set $color-yellow  #FAC863
set $color-red  #EC5F67
set $color00  #1B2B34
set $color01  #EC5F67
set $color09  #EC5F67
set $color02  #99C794
set $color10 #99C794
set $color03  #FAC863
set $color11 #FAC863
set $color04  #6699CC
set $color12 #6699CC
set $color05  #C594C5
set $color13 #C594C5
set $color06  #5FB3B3
set $color14 #5FB3B3
set $color07  #C0C5CE
set $color15 #C0C5CE
set $color08  #65737E
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
# set $menu dmenu_path | dmenu | xargs swaymsg exec --
set $menu bemenu-run --fn "monospace 18" -b -p "▶" --tf "$prompt" --hf "$highlight" --sf "$highlight" --scf "$highlight" | xargs swaymsg exec

set $wallpaper ~/Pictures/walls/koi.jpg

set $lock swaylock -f \
        -i $wallpaper -s fill \
        --font "Clear Sans" --font-size 69 \
        --indicator-radius 160 \
        --indicator-thickness 40 \
        --inside-color 00000000 \
        --inside-clear-color 00000000 \
        --inside-ver-color 00000000 \
        --inside-wrong-color EC5F67 \
        --layout-bg-color 1B2B34 \
        --line-color 99c794 \
        --ring-color 99c794 \
        --ring-ver-color 99c794


default_border pixel 4
gaps inner 5
gaps outer 0
default_floating_border pixel
hide_edge_borders smart

### Output configuration
#

# Wallpaper
output * bg $wallpaper fill

# or dynamic wall
# exec /home/dab/src/sunpaper/sunpaper.sh 

#
# Example configuration:
#
output eDP-1 resolution 3840x2400 scale 2 position 0,1440
output HDMI-A-1 resolution 2560x1440 scale 1 position 0,0
# exec kanshi
exec wlsunset -l 60.1 -L 24.7
exec_always nwg-panel
# exec systemctl --user start pulseaudio
# exec pa-applet &
# exec /opt/piavpn/bin/pia-client &
exec autotiling


# Ensure xdg-desktop-portal-wlr knows which wayland instance to use
exec systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP

#exec_always /home/dab/.bin/laptop-mode.sh
exec_always export XDG_CURRENT_DESKTOP=unity
exec_always export XDG_SESSION_TYPE=wayland

exec dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway

exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
exec gnome-keyring-daemon --start --components=pkcs11 &
#exec eval $(gnome-keyring-daemon --start)
exec export SSH_AUTH_SOCK

#


# You can get the names of your outputs by running: swaymsg -t get_outputs
set $term_run foot -e zsh -c
bindsym $mod+Shift+w exec $term_run "swaymsg -t get_outputs | jq -r '.[] | select(.focused!=true) | [.name, .make, .model] | @tsv' | fzf --select-1 --reverse --prompt='Move workspace to:' | awk '{print $1}' | xargs -r swaymsg move workspace to"
bindsym $mod+Alt+w exec swaymsg -t get_outputs | jq '.[] | select(.focused!=true) | .name' | head -n1 | xargs swaymsg move workspace to

### Idle configuration
#
# Example configuration:
#
 exec swayidle -w \
          timeout 300 $lock\
          timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
          before-sleep $lock
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.


### Input configuration
#
# Example configuration:
#
input "1739:52823:SYNA8009:00_06CB:CE57_Touchpad" {
    dwt enabled
    tap enabled
    natural_scroll enabled
    middle_emulation enabled
}

input * {
    xkb_layout "us,ru",
    xkb_options "ctrl:nocaps,grp:rctrl_toggle,grp_led:caps" 
}

input 1:1:AT_Translated_Set_2_keyboard repeat_delay 300
input 1:1:AT_Translated_Set_2_keyboard repeat_rate 60

#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Kill focused window
    bindsym $mod+q kill

    # Start your launcher
    bindsym $mod+p exec $menu
    bindsym $mod+d exec rofi_run

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+c reload

    # Exit sway (logs you out of your Wayland session)

    for_window [window_role="pop-up"] floating enable
    for_window [window_role="bubble"] floating enable
    for_window [window_role="task_dialog"] floating enable
    for_window [window_role="Preferences"] floating enable
    for_window [window_type="dialog"] floating enable
    for_window [window_type="menu"] floating enable
    for_window [window_role="About"] floating enable
    for_window [class="xdg-desktop-portal-kde"] floating enable
    for_window [class="ksysguard"] floating enable
    for_window [class="ksysguard"] sticky enable
    for_window [app_id="mpv"] floating enable
    #for_window [app_id="mpv"] layout tabbed
    for_window [class="Wine"] floating enable
    for_window [app_id="lutris"] floating enable

    bindsym $mod+F12 exec shutdown now
    bindsym $mod+Escape exec $lock
    #bindsym Control+Mod1+Delete exec ksysguard
    #bindsym $mod+F1 exec dolphin
    #bindsym $mod+F2 exec cantata
    bindsym $mod+F3 exec mpv --player-operation-mode=pseudo-gui
    #bindsym $mod+F4 exec firefox
    #bindsym $mod+F5 exec kate
    #bindsym $mod+F6 exec lutris
    bindsym $mod+F7 exec notify-send $(weather)
    #bindsym $mod+F8 exec pkill kmousetool || kmousetool
    bindsym Print exec grim -o $(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') $(xdg-user-dir)/Pictures/Screenshot_$(date +'%Y-%m-%d-%H%M%S.png')
    bindsym Alt+Print exec grim $(xdg-user-dir)/Pictures/Screenshot_$(date +'%Y-%m-%d-%H%M%S.png')
    bindsym Control+Print exec grim -g "$(slurp)" $(xdg-user-dir)/Pictures/Screenshot_$(date +'%Y-%m-%d-%H%M%S.png')
    bindsym Shift+Print exec grim -g "$(slurp)" - | wl-copy
    bindsym $mod+tab workspace back_and_forth

    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

#
# Multimedia keys
#

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 3 +5% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 3 -5% #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound
bindsym $mod+Insert exec --no-startup-id pactl set-sink-volume 3 +5% #increase sound volume
bindsym $mod+Delete exec --no-startup-id pactl set-sink-volume 3 -5% #decrease sound volume

bindsym XF86MonBrightnessDown exec brightnessctl set 5%-
bindsym XF86MonBrightnessUp exec brightnessctl set +5%

titlebar_border_thickness 2
titlebar_padding 5 5
title_align left

# Colors:               border     background  text      indicator  child border
client.focused          $color12   $color12    $background  $color04   $color04
client.focused_inactive $color12   $color00    $color12  $color12   $color00
client.unfocused        $color00   $background $color07  $color04   $color00
client.urgent           $color01   $background $color07  $color04   $color01


#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    position top
    mode invisible
    font monospace 9

    # When the status_command prints a new line to stdout, swaybar updates.
    # The default just shows the current date and time.
    status_command while date +'%Y-%m-%d %l:%M:%S %p'; do sleep 1; done

    colors {
        statusline $foreground
        background $background
        #background #00000000
        focused_workspace $background $prompt #232627
        active_workspace $background $highlight #232627
        inactive_workspace $background #65737E #232627
    }
}

xwayland disable


include /etc/sway/config.d/*
